package com.example.rezazr.bkk.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Siswa {
    @SerializedName("nis")
    @Expose
    private String nis;
    @SerializedName("id_pendaftaran")
    @Expose
    private String idPendaftaran;
    @SerializedName("id_bio")
    @Expose
    private String idBio;
    @SerializedName("tgl_masuk")
    @Expose
    private String tglMasuk;
    @SerializedName("id_keahlian")
    @Expose
    private String idKeahlian;
    @SerializedName("nisn")
    @Expose
    private String nisn;
    @SerializedName("no_peserta_un")
    @Expose
    private Object noPesertaUn;
    @SerializedName("siswa_aktif")
    @Expose
    private String siswaAktif;
    @SerializedName("id_tahun_pelajaran")
    @Expose
    private String idTahunPelajaran;
    @SerializedName("nama_lengkap")
    @Expose
    private String namaLengkap;
    @SerializedName("jenis_kelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("tempat_lahir")
    @Expose
    private String tempatLahir;
    @SerializedName("tanggal_lahir")
    @Expose
    private String tanggalLahir;
    @SerializedName("agama")
    @Expose
    private String agama;
    @SerializedName("alamat_jalan")
    @Expose
    private String alamatJalan;
    @SerializedName("alamat_rt")
    @Expose
    private String alamatRt;
    @SerializedName("alamat_rw")
    @Expose
    private String alamatRw;
    @SerializedName("alamat_desa")
    @Expose
    private String alamatDesa;
    @SerializedName("alamat_kecamatan")
    @Expose
    private String alamatKecamatan;
    @SerializedName("alamat_kota")
    @Expose
    private String alamatKota;
    @SerializedName("alamat_pos")
    @Expose
    private String alamatPos;
    @SerializedName("telp_rumah")
    @Expose
    private String telpRumah;
    @SerializedName("telp_mobile")
    @Expose
    private String telpMobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("berkas")
    @Expose
    private Object berkas;
    @SerializedName("nama_keahlian")
    @Expose
    private String namaKeahlian;
    @SerializedName("parent")
    @Expose
    private String parent;
    @SerializedName("kategori")
    @Expose
    private String kategori;
    @SerializedName("foto")
    @Expose
    private String foto;

    public String getNis() {
        return nis;
    }

    public void setNis(String nis) {
        this.nis = nis;
    }

    public String getIdPendaftaran() {
        return idPendaftaran;
    }

    public void setIdPendaftaran(String idPendaftaran) {
        this.idPendaftaran = idPendaftaran;
    }

    public String getIdBio() {
        return idBio;
    }

    public void setIdBio(String idBio) {
        this.idBio = idBio;
    }

    public String getTglMasuk() {
        return tglMasuk;
    }

    public void setTglMasuk(String tglMasuk) {
        this.tglMasuk = tglMasuk;
    }

    public String getIdKeahlian() {
        return idKeahlian;
    }

    public void setIdKeahlian(String idKeahlian) {
        this.idKeahlian = idKeahlian;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public Object getNoPesertaUn() {
        return noPesertaUn;
    }

    public void setNoPesertaUn(Object noPesertaUn) {
        this.noPesertaUn = noPesertaUn;
    }

    public String getSiswaAktif() {
        return siswaAktif;
    }

    public void setSiswaAktif(String siswaAktif) {
        this.siswaAktif = siswaAktif;
    }

    public String getIdTahunPelajaran() {
        return idTahunPelajaran;
    }

    public void setIdTahunPelajaran(String idTahunPelajaran) {
        this.idTahunPelajaran = idTahunPelajaran;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getAlamatJalan() {
        return alamatJalan;
    }

    public void setAlamatJalan(String alamatJalan) {
        this.alamatJalan = alamatJalan;
    }

    public String getAlamatRt() {
        return alamatRt;
    }

    public void setAlamatRt(String alamatRt) {
        this.alamatRt = alamatRt;
    }

    public String getAlamatRw() {
        return alamatRw;
    }

    public void setAlamatRw(String alamatRw) {
        this.alamatRw = alamatRw;
    }

    public String getAlamatDesa() {
        return alamatDesa;
    }

    public void setAlamatDesa(String alamatDesa) {
        this.alamatDesa = alamatDesa;
    }

    public String getAlamatKecamatan() {
        return alamatKecamatan;
    }

    public void setAlamatKecamatan(String alamatKecamatan) {
        this.alamatKecamatan = alamatKecamatan;
    }

    public String getAlamatKota() {
        return alamatKota;
    }

    public void setAlamatKota(String alamatKota) {
        this.alamatKota = alamatKota;
    }

    public String getAlamatPos() {
        return alamatPos;
    }

    public void setAlamatPos(String alamatPos) {
        this.alamatPos = alamatPos;
    }

    public String getTelpRumah() {
        return telpRumah;
    }

    public void setTelpRumah(String telpRumah) {
        this.telpRumah = telpRumah;
    }

    public String getTelpMobile() {
        return telpMobile;
    }

    public void setTelpMobile(String telpMobile) {
        this.telpMobile = telpMobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getBerkas() {
        return berkas;
    }

    public void setBerkas(Object berkas) {
        this.berkas = berkas;
    }

    public String getNamaKeahlian() {
        return namaKeahlian;
    }

    public void setNamaKeahlian(String namaKeahlian) {
        this.namaKeahlian = namaKeahlian;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
