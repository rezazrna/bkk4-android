package com.example.rezazr.bkk.Model;

import com.google.gson.annotations.SerializedName;

public class File {
    @SerializedName("response")
    private String response;

    public String getResponse() {
        return response;
    }
}
