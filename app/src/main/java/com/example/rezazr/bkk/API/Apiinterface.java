package com.example.rezazr.bkk.API;

import com.example.rezazr.bkk.Model.File;
import com.example.rezazr.bkk.Model.LoginResponses;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Apiinterface {

      @Multipart
      @POST("file.php")
      Call<com.example.rezazr.bkk.Model.File> PdfUploadFunction (@Part("title") RequestBody title,
                                                                 @Part("id_job") RequestBody id_job,
                                                                 @Part("name") RequestBody name,
                                                                 @Part("date") RequestBody date,
                                                                 @Part MultipartBody.Part pdf
                                                                );
        @POST("auth/login")
        @FormUrlEncoded
        Observable<LoginResponses> getProfile(@Field("username")String username,
                                              @Field("password")String password,
                                              @Field("token")String token,
                                              @Field("client_id")String client_id,
                                              @Field("client_server")String client_server);


        @GET("api/siswa/{nis}?detail=true")
        Observable<LoginResponses> getDetail(@Path("nis") String nis, @Header("Authorization") String auth);

}


