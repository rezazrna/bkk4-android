package com.example.rezazr.bkk;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

public class JobAdapter extends RecyclerView.Adapter<JobAdapter.JobViewHolder> {
    private Context mCtx;
    private List<Job> jobList;

    public JobAdapter(Context mCtx, List<Job> jobList) {
        this.mCtx = mCtx;
        this.jobList = jobList;
    }

    @Override
    public JobViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.job_list, null);
        return new JobViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JobViewHolder holder, int position) {
        final Job job = jobList.get(position);

        //loading the image
        Glide.with(mCtx)
                .load(job.getPict())
                .into(holder.ivpict);

        holder.tvposition.setText(job.getPosition());
        holder.tvcompany.setText(job.getCompany());
        holder.tvcity.setText(String.valueOf(job.getCity()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mCtx,DetailActivity.class);
                Log.d("TAG", "ID Job " + job.getId_job());
                intent.putExtra("id_job",job.getId_job());
                intent.putExtra("position",job.getPosition());
                intent.putExtra("company",job.getCompany());
                intent.putExtra("city",job.getCity());
                intent.putExtra("pict",job.getPict());
                intent.putExtra("location",job.getLocation());
                intent.putExtra("requirement",job.getRequirement());
                intent.putExtra("jobdesc",job.getJobdesc());
                mCtx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    class JobViewHolder extends RecyclerView.ViewHolder {

        TextView tvposition, tvcompany, tvcity;
        ImageView ivpict;

        public JobViewHolder(View itemView) {
            super(itemView);

            tvposition = itemView.findViewById(R.id.tvposition);
            tvcompany = itemView.findViewById(R.id.tvcompany);
            tvcity = itemView.findViewById(R.id.tvcity);
            ivpict = itemView.findViewById(R.id.ivpict);
        }
    }
}
