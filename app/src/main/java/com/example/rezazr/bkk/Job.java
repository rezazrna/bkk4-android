package com.example.rezazr.bkk;

public class Job {
    private int id_job;
    private String position;
    private String company;
    private String city;
    private String pict;
    private String location;
    private String requirement;
    private String jobdesc;

    public Job(int id_job, String position, String company, String city, String pict, String location, String requirement, String jobdesc) {
        this.id_job = id_job;
        this.position = position;
        this.company = company;
        this.city = city;
        this.pict = pict;
        this.location = location;
        this.requirement = requirement;
        this.jobdesc = jobdesc;
    }

    public int getId_job() {
        return id_job;
    }

    public String getPosition() {
        return position;
    }

    public String getCompany() {
        return company;
    }

    public String getCity() {
        return city;
    }

    public String getPict() {
        return pict;
    }

    public String getLocation() { return location; }

    public String getRequirement() {return  requirement; }

    public String getJobdesc() { return jobdesc; }
}
