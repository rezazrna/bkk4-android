package com.example.rezazr.bkk;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.rezazr.bkk.API.ApiServices;
import com.example.rezazr.bkk.API.ApiServicesLogin;
import com.example.rezazr.bkk.API.Apiinterface;
import com.example.rezazr.bkk.Model.LoginResponses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ProfileActivity extends AppCompatActivity {

    private ImageView ivpictuser;
    private TextView tvusername, tvname, tvkeahlian, tvpassword;
    ApiServicesLogin retrofit;
    Disposable disposable;
    Config config;
    private Context Ctx;
    String value = "";
    String value2 = "";
//    String nis = i.getStringExtra("nis");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Ctx = this;
        SharedPreferences setting = getSharedPreferences(Config.SHARED_PREF_NAME,MODE_PRIVATE);
        value = setting.getString(Config.USERNAME_SHARED_PREF, "username");
        value2 = setting.getString(Config.PASSWORD_SHARED_PREF, "password");
        ivpictuser = (ImageView)findViewById(R.id.ivpictuser);
        tvusername = (TextView)findViewById(R.id.tvusername);
        tvname = (TextView)findViewById(R.id.tvname);
        tvkeahlian = (TextView)findViewById(R.id.tvkeahlian);
        tvpassword = (TextView)findViewById(R.id.tvpassword);
        Intent i = getIntent();
        Apiinterface api = retrofit.getKoneksi().create(Apiinterface.class);
         api.getDetail(value , "Bearer " + "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLnNjaG9vbGJvb3N0ZXIuYWd5c29uLmNvbS9hdXRoL2xvZ2luIiwiaWF0IjoxNTQ4MjIyOTkwLCJuYmYiOjE1NDgyMjI5OTAsImp0aSI6IjJ1V1NOMEx5ak1Hb284S3MiLCJzdWIiOjgxOSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.187clxG_9GmanhItPxPh4jPN-c1miC2ve8tGrnkuoj0").subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<LoginResponses>() {
                    @Override
                    public void onNext(LoginResponses loginResponses) {
                                //Toast.makeText(getApplicationContext(),""+loginResponses.data.getNama(),Toast.LENGTH_SHORT).show();
                        tvusername.setText(value);
                        tvpassword.setText(value2);
                        tvname.setText(loginResponses.getSiswa().getNamaLengkap());
                        tvkeahlian.setText(loginResponses.getSiswa().getNamaKeahlian());
                        if(!loginResponses.getSiswa().getFoto().isEmpty()){
                            Glide.with(getApplicationContext())
                                .load(loginResponses.getSiswa().getFoto())
                                .into(ivpictuser);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });



    }
}
