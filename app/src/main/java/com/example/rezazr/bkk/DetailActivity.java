package com.example.rezazr.bkk;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.rezazr.bkk.API.ApiServices;
import com.example.rezazr.bkk.API.Apiinterface;
import com.obsez.android.lib.filechooser.ChooserDialog;

import java.io.File;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.util.Calendar;

public class DetailActivity extends AppCompatActivity {
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private ImageView ivpictdetail;
    private TextView tvcompanydetail, tvpositiondetail, tvcitydetail, tvlocation, tvrequirement, tvjobdesc, tvfile;
    private Button btnapply, btnsubmit;
    String PdfNameHolder, PdfPathHolder, PdfID;

    Uri uri = null;
    String value = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ivpictdetail = (ImageView) findViewById(R.id.ivpictdetail);
        tvcompanydetail = (TextView) findViewById(R.id.tvcompanydetail);
        tvpositiondetail = (TextView) findViewById(R.id.tvpositiondetail);
        tvcitydetail = (TextView) findViewById(R.id.tvcitydetail);
        tvlocation = (TextView) findViewById(R.id.tvlocation);
        tvrequirement = (TextView) findViewById(R.id.tvrequirement);
        tvjobdesc = (TextView) findViewById(R.id.tvjobdesc);
        btnapply = findViewById(R.id.btnapply);
        btnsubmit = findViewById(R.id.btnsubmit);
        tvfile = (TextView) findViewById(R.id.tvfile);
        Intent i = getIntent();

        String id_job = i.getStringExtra("id_job");
        String pict = i.getStringExtra("pict");
        String company = i.getStringExtra("company");
        String position = i.getStringExtra("position");
        String city = i.getStringExtra("city");
        String location = i.getStringExtra("location");
        String requirement = i.getStringExtra("requirement");
        String jobdesc = i.getStringExtra("jobdesc");

        tvcompanydetail.setText(company);
        tvpositiondetail.setText(position);
        tvcitydetail.setText(city);
        tvlocation.setText(location);
        tvrequirement.setText(requirement);
        tvjobdesc.setText(jobdesc);

        Glide.with(this)
                .load(pict)
                .into(ivpictdetail);

        btnapply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               chooser();
            }
        });

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PdfUploadFunction();
            }
        });
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == Activity.RESULT_OK) {
//            String fileName = "";
//            if (data != null) {
//                uri = data.getData();
//                tvfile.setText(getFileName(uri));
//                Toast.makeText(getApplicationContext(),getFullPath(getApplicationContext(),uri),Toast.LENGTH_LONG).show();
//            }
//        }
//    }

    String filepath = "";

//    public String getFullPath(Context context, Uri uri) {
//        Cursor cursor = null;
//        try{
//            String[] proj = {MediaStore.Files.FileColumns.DATA};
//            cursor =context.getContentResolver().query(uri,  proj, null, null, null);
//            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA);
//            cursor.moveToFirst();
//            return cursor.getString(column_index);
//        } finally {
//            if (cursor != null) {
//                cursor.close();
//            }
//        }
//
//    }

    File file;
    String namafile = "",extension = null;
    public void chooser(){
        new ChooserDialog().with(this)
                .withStartFile("/storage/emulated/0/")
                .withChosenListener(new ChooserDialog.Result() {
                    @Override
                    public void onChoosePath(String path, File pathFile) {
                        file = pathFile;
                        namafile = pathFile.getName();
                        extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
                        tvfile.setText(namafile);
                        Log.wtf("l", extension);
                    }
                })
                .build()
                .show();
    }

//    public String getFileName(Uri uri) {
//        String result = null;
//        if (uri.getScheme().equals("content")) {
//            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
//            try {
//                if (cursor != null && cursor.moveToFirst()) {
//                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
//                    //filepath = cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA));
//                }
//            } finally {
//                cursor.close();
//            }
//        }
//        if (result == null) {
//            result = uri.getPath();
//            int cut = result.lastIndexOf("/");
//            if (cut != -1) {
//                result = result.substring(cut + 1);
//            }
//        }
//        return result;
//    }

//    private static final int FILE_SELECT_CODE = 0;
//
//    public void showFileChooser() {
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.setType("application/pdf");
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//        try {
//            startActivityForResult(Intent.createChooser(intent, "Select File"), FILE_SELECT_CODE);
//        } catch (android.content.ActivityNotFoundException ex) {
//            Toast.makeText(getApplicationContext(), "You Don't Have File Manager", Toast.LENGTH_LONG).show();
//        }
//    }

    public void PdfUploadFunction() {

        Intent i = getIntent();
        int id_job = i.getIntExtra("id_job", 0);
        SharedPreferences setting = getSharedPreferences(Config.SHARED_PREF_NAME,MODE_PRIVATE);
        value = setting.getString(Config.NAME_SHARED_PREF,"name");
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String todayString = formatter.format(todayDate);

        PdfNameHolder = tvfile.getText().toString();

        PdfPathHolder = file.getPath();

        //File file=new File(uri.getPath());
        Apiinterface apiInterface= ApiServices.getKoneksi().create(Apiinterface.class);

       RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"),file);
       MultipartBody.Part multiPart = MultipartBody.Part.createFormData("pdf",file.getName(),requestFile);

       Call<com.example.rezazr.bkk.Model.File> service = apiInterface.PdfUploadFunction(generate(PdfNameHolder) , generate(String.valueOf(id_job)), generate(value), generate(todayString), multiPart);
       service.enqueue(new Callback<com.example.rezazr.bkk.Model.File>() {
           @Override
           public void onResponse(Call<com.example.rezazr.bkk.Model.File> call, Response<com.example.rezazr.bkk.Model.File> response) {

                   Toast.makeText(getApplicationContext(),"Uploaded "+response.body().getResponse(),Toast.LENGTH_LONG).show();

           }

           @Override
           public void onFailure(Call<com.example.rezazr.bkk.Model.File> call, Throwable t) {
                Log.e("connection",t.getMessage());
               Toast.makeText(getApplicationContext(),"Error "+t.getMessage(),Toast.LENGTH_LONG).show();
           }
       });

//        RequestBody title=RequestBody.create(MediaType.parse("text/plain"),PdfNameHolder);
//        RequestBody pdf =RequestBody.create(MediaType.parse("*/*"), file);
//        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("pdf",file.getName(),pdf);
//        RequestBody id = RequestBody.create(MediaType.parse("text/plain"),"1");
//        RequestBody date = RequestBody.create(MediaType.parse("text/plain"),""+currentTime);

//        Call<com.example.rezazr.bkk.Model.File> call = apiInterface.PdfUploadFunction(title,fileToUpload,id,date);
//        call.enqueue(new Callback<com.example.rezazr.bkk.Model.File>() {
//            @Override
//            public void onResponse(Call<com.example.rezazr.bkk.Model.File> call, Response<com.example.rezazr.bkk.Model.File> response) {
//                if(response.isSuccessful()){
//                    Toast.makeText(getApplicationContext(),"SUKSES",Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<com.example.rezazr.bkk.Model.File> call, Throwable t) {
//
//            }
//        });
    }

    RequestBody generate(String text) {
        return RequestBody.create(MediaType.parse("text/plain"), text);
    }


    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}
