package com.example.rezazr.bkk;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.cenkgun.chatbar.ChatBarView;

public class ChatActivity extends AppCompatActivity {

    TextView message;
    ChatBarView chatbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        message = (TextView)findViewById(R.id.message);
        chatbar = (ChatBarView)findViewById(R.id.chatbar);

        chatbar.setMessageBoxHint("Enter Your Message");
        chatbar.setSendClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message.setText(chatbar.getMessageText());
            }
        });
    }
}
